import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { TopicDetailsComponent } from './topic-details/topic-details.component';

import { DataService } from './data.service';
import { TopicsComponent } from './topics/topics.component';
import { CreateTopicComponent } from './create-topic/create-topic.component';

import { FormsModule } from '@angular/forms';
import { UserListComponent } from './user-list/user-list.component';


@NgModule({
  declarations: [
    AppComponent,
    TopicDetailsComponent,
    TopicsComponent,
    CreateTopicComponent,
    UserListComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
