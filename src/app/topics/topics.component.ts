import { Component, OnInit } from '@angular/core';
import { Topic } from '../topic';
import { DataService } from '../data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '../app.component';
//import { Location } from '@angular/common';

@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.css']
})


export class TopicsComponent implements OnInit {

  topics: Topic[];


  constructor(private dataService: DataService, private router: Router,
              private route: ActivatedRoute, private appComponent: AppComponent
              ) { }

  getTopics(){
    return this.dataService.getTopics().then(topics => this.topics = topics);
  }

  topicDetails(id: number) {
    console.log('id = ' + id);
    this.router.navigate(['/details', id]);
  }

  deleteTopic(id: string){
    //this.dataService.delete(id).then(()=> this.goBack());
    this.dataService.delete(id);
  }

  ngOnInit(): void {
    this.appComponent.userList = false
    this.route.paramMap.subscribe(params => {
       var userid:string = this.route.snapshot.paramMap.get('id');
       this.appComponent.userId = userid
       this.dataService.getTopicByUserId(userid).then(topic => this.topics = topic);
    });
    //this.getTopics();
  }

  /* goBack(): void {
    this.location.back();
  } */

}
