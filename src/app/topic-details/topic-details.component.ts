import { Component, Input, OnInit } from '@angular/core';

import { Topic } from '../topic';

import { enableProdMode } from '@angular/core';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

enableProdMode();

@Component({
  selector: 'app-topic-details',
  templateUrl: './topic-details.component.html',
  styleUrls: ['./topic-details.component.css']
})

export class TopicDetailsComponent implements OnInit {

  topic: Topic;
  //topic = new Topic();
  submitted = false;
  topicBool = true;
  oldid:string
  constructor(
    private dataService: DataService,
    private route: ActivatedRoute,
    private location: Location) { }

//@Input() topic: Topic;


  ngOnInit(): void {
     this.route.paramMap.subscribe(params => {
       this.oldid = this.route.snapshot.paramMap.get('id');
       this.dataService.getTopic(this.oldid).then(topic => this.topic = topic);
    });
  }

  deleteTopic(): void{
      this.submitted = true;
      this.topicBool = false;
    if( this.dataService.delete(this.topic.id)==null){
        this.topicBool = false;
      } else{
        this.topicBool = false;
      }
  }
  
  onSubmit(): void {
    this.submitted = true;
    this.topicBool = true;
    const str = this.topic.id.replace(/\s+/g, '');
    this.topic.id = str.toLowerCase();
    this.dataService.update(this.topic, this.oldid);
  }

  goBack(): void {
    this.location.back();
  }
}
