export class Topic {


  constructor( public id: string,
               public name: string,
               public description: string,
              public userid: String) {
  }
}
