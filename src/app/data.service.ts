import { Injectable } from '@angular/core';

import { Topic } from './topic';

import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';

import {Observable} from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class DataService {

    // private topicsUrl = 'api/topics';
    private topicsUrl = 'http://java-microservice-app1-1289745246.us-west-2.elb.amazonaws.com:8181/api/topics';
    private usersUrl = 'http://java-microservice-app1-1289745246.us-west-2.elb.amazonaws.com:9999/api/user';
    //private topicsUrl = 'http://localhost:8181/api/topics';
    //private usersUrl = 'http://localhost:9999/api/user';
    private headers = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(private http: HttpClient) { }

    getTopics(): Promise<Topic[]> {
     return this.http.get(this.topicsUrl)
    .toPromise()
    .then(response => response as Topic[])
    .catch(this.handleError);
  }

  getUsers(): Promise<User[]> {
     return this.http.get(this.usersUrl + "/allUsers")
    .toPromise()
    .then(response => response as User[])
    .catch(this.handleError);
  }

   getTopic(id: string): Promise<Topic> {
      const url = `${this.topicsUrl}/${id}`;
      return this.http.get(url)
      .toPromise()
      .then(Response => Response as Topic)
      .catch(this.handleError);
  }

  getTopicByUserId(id: string): Promise<Topic[]> {
      const url = `${this.usersUrl+"/topicByUserId"}/${id}`;
      //const url = this.usersUrl+"/topicByUserId"
      return this.http.get(url)
      .toPromise()
      .then(Response => Response as Topic[])
      .catch(this.handleError);
  }


  create(topic: Topic): Promise<Topic> {
    console.log(JSON.stringify(topic))
    console.log(JSON.parse(JSON.stringify(topic)))
    return this.http
      .post(this.topicsUrl, JSON.parse(JSON.stringify(topic)), {headers: this.headers})
      .toPromise()
      //.then(res => res as Topic[])
      .catch(this.handleError);
  }

  update(topic: Topic, id: string): Promise<Topic> {
    const url = `${this.topicsUrl}/${id}`;
    return this.http
      .put(url, JSON.parse(JSON.stringify(topic)), {headers: this.headers})
      .toPromise()
      .then()
      .catch(this.handleError);
  }

  delete(id: string): Promise<void> {
    const url = `${this.topicsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('Error', error);
    return Promise.reject(error.message || error);
  }
}
