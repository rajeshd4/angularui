import { Topic } from './topic';

export class User {


  constructor( public id: string,
               public username: string,
               public topicList: string) {
  }
}