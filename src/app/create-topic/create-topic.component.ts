import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Location } from '@angular/common';
import { Topic } from '../topic';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-create-topic',
  templateUrl: './create-topic.component.html',
  styleUrls: ['./create-topic.component.css']
})
 export class CreateTopicComponent implements OnInit {
  id:string
  name:string
  description:string
  userid:string
  topic: Topic;
  submitted = false;

  constructor(private dataService: DataService,
              private location: Location, private appComponent: AppComponent) { }

  ngOnInit() {
    this.appComponent.userList = false
  }

  private save(): void {
   this.dataService.create(this.topic);
 }

 onSubmit() {
  const str = this.id.replace(/\s+/g, '');
  this.topic = new Topic(str.toLowerCase(), this.name, this.description, this.userid);
  this.submitted = true;
  this.save();
 }

 goBack(): void {
    this.location.back();
  }

}
