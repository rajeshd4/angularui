import { TopicDetailsComponent } from './topic-details/topic-details.component';
import { TopicsComponent } from './topics/topics.component';
import { CreateTopicComponent } from './create-topic/create-topic.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [
   { path: '', redirectTo: '/users', pathMatch: 'full' },
   { path: 'topics/:id', component: TopicsComponent },
   { path: 'detail/:id', component: TopicDetailsComponent },
   { path: 'add', component: CreateTopicComponent },
   { path: 'users', component: UserListComponent }
   //{ path: 'topics',  component: TopicsComponent },
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}

