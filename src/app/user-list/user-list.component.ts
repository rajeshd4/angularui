import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../data.service';
import { User } from '../user';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  users: User[]

  constructor(private dataService: DataService, private router: Router, 
    private appComponent: AppComponent) { }

  ngOnInit() {
    this.appComponent.userList = true
    this.getUsers()
  }

  getUsers(){
    return this.dataService.getUsers().then(users => this.users = users);
  }

}
