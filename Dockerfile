# base image
#FROM node:10.15.3
#FROM node:12.7-alpine AS build

# set working directory
#WORKDIR /angularui


# add `/app/node_modules/.bin` to $PATH
#ENV PATH /angularui/node_modules/.bin:$PATH

# install and cache app dependencies
#COPY package.json /angularui/package.json

#RUN npm install
#RUN npm install -g @angular/cli@8.3.25

# start app
#CMD ng serve --open



#FROM node:12.7-alpine AS builder

#COPY . ./angularui
#WORKDIR /angularui
#RUN npm i
#RUN $(npm bin)/ng build --prod




#FROM node:12.7-alpine AS builder
#COPY . ./angularui
#WORKDIR /angularui
#RUN npm i
#RUN $(npm bin)/ng build --prod


#FROM nginx:1.15.8-alpine
#EXPOSE 4200
#COPY --from=builder /angularui/dist/angularui /usr/share/nginx/html

# STAGE 1: Build ###
FROM node:12.7-alpine AS build
WORKDIR /usr/src/app
COPY package.json ./
RUN npm install
COPY . .
RUN npm run build
### STAGE 2: Run ###
FROM nginx:1.17.1-alpine
COPY --from=build /usr/src/app/dist/angularui /usr/share/nginx/html




